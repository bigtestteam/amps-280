package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.FilterTestPluginDescriptorMojo;

import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "filter-test-plugin-descriptor")
public class StashFilterTestPluginDescriptorMojo extends FilterTestPluginDescriptorMojo
{
}
